#include "stdafx.h"
#include <iostream>

class matrix
{
protected:
	double ma[10][10];
	int rows;
	int	columns;
public:
	matrix();
	~matrix();
	bool plus(matrix ma2);
	bool transp();
	bool mult(matrix ma2);
	virtual bool input();
	void print();

	int getrows()
	{
		return rows;
	}

	int getcolumns()
	{
		return columns;
	}

	double getelem(int r, int c)
	{
		if (r < rows && c<columns && c>-1)
			return ma[r][c];
		std::cout << "CANNOT GET ELEMENT. ERROR\n";
		return -1;
	}
};

matrix::matrix()
{

}
matrix::~matrix()
{

}
bool matrix::plus(matrix ma2)
{
	if (rows != ma2.getrows())
		return false;
	if (columns != ma2.getcolumns())
		return false;

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
			ma[i][j] += ma2.getelem(i, j);
	}
	return true;
}
bool matrix::input()
{
	int rs;
	int cs;
	std::cout << "Rows = ";
	std::cin >> rs;
	if (rs < 0)
	{
		std::cout << "ERROR ROWS  \n" << std::endl;
		return false;
	}
	if (rs > 10)
	{
		std::cout << "ERROR ROWS  \n" << std::endl;
		return false;
	}
	std::cout << "Columns = ";
	std::cin >> cs;
	std::cout << "\n";
	if (cs < 0)
	{
		std::cout << "ERROR COLUMNS  \n" << std::endl;
		return false;
	}
	if (cs > 10)
	{
		std::cout << "ERROR COLUMNS  \n" << std::endl;
		return false;
	}

	for (int i = 0; i < rs; i++)
		for (int j = 0; j < cs; j++)
			std::cin >> ma[i][j];
	columns = cs;
	rows = rs;
	std::cout << "\n";
	return true;
}

void matrix::print()
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
			std::cout << ma[i][j] << "\t";
		std::cout << std::endl;
	}
	std::cout << "\n";
}

bool matrix::transp()
{
	double ma3[10][10];

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			ma3[i][j] = ma[i][j];
		}
	}

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			ma[j][i] = ma3[i][j];
		}
	}
	int temp = rows;
	rows = columns;
	columns = temp;

	return true;
}

bool matrix::mult(matrix ma2)
{

	if (columns != ma2.getrows())
	{
		std::cout << "ERROR \n" << std::endl;
		return false;
	}

	double ma3[10][10];

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			ma3[i][j] = ma[i][j];
		}
	}

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < ma2.getcolumns(); j++)
		{
			double x = 0.0;
			for (int t = 0; t < columns; t++)
			{
				x += ma3[i][t] * ma2.getelem(t, j);
			}
			ma[i][j] = x;

		}
	}
	columns = ma2.getcolumns();
	return true;
}


class vector : public matrix
{
public:

	vector();
	double scalmultvector(vector vect2);
	void xmult(double k);
	virtual bool input();
};

bool vector::input()
{
	int row = 1;
	int col;
	std::cout << "Rows = 1" << std::endl;
	if (row < 0)
		return false;
	if (row > 10)
		return false;
	std::cout << "Columns = ";
	std::cin >> col;
	for (int i = 0; i < row; i++)
		for (int j = 0; j < col; j++)
			std::cin >> ma[i][j];
	columns = col;
	rows = row;
	std::cout << "\n";
	return true;
}


vector::vector()
{
}

double vector::scalmultvector(vector vect2)
{
	vector tmp = *this;
	if (tmp.mult(vect2))
	{
		return tmp.getelem(0, 0);
	}
	tmp = *this;
	tmp.transp();
	if (tmp.mult(vect2))
	{
		return tmp.getelem(0, 0);
	}
	return -1;
}

void vector::xmult(double n)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			ma[i][j] *= n;
		}
	}
}

int main()
{
	std::cout << "lab6\n\n";
	std::cout << "Do not enter numbers greater than 10\n\n";
	matrix ma1;
	std::cout << "Input first mass\n\n";
	ma1.input();
	std::cout << "First mass = \n";
	ma1.print();
	matrix ma2;
	std::cout << "Input second mass\n\n";
	ma2.input();
	std::cout << "Second mass = \n";
	ma2.print();
	ma1.plus(ma2);
	std::cout << "First mass + second mass = \n";
	ma1.print();
	ma1.transp();
	std::cout << "First mass transposed = \n";
	ma1.print();
	std::cout << "First mass * second mass = \n";
	ma1.mult(ma2);
	ma1.print();
	vector mv1;
	vector mv2;
	std::cout << "Input first vector\n\n";
	mv1.input();
	std::cout << "First vector = \n";
	mv1.print();
	std::cout << "Input second vector\n\n";
	mv2.input();
	std::cout << "Second vector = \n";
	mv2.print();
	mv1.transp();
	std::cout << "First vector transposed = \n";
	mv1.print();
	mv1.xmult(5);
	std::cout << "First vector * 5 = \n";
	mv1.print();
	mv1.plus(mv2);
	mv1.print();
	double f = mv1.scalmultvector(mv2);
	std::cout << "ScalMult =\t" << f << std::endl;
	getchar();
	return 0;
}
