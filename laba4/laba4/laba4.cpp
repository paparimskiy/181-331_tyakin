#include "stdafx.h" 
#include <iostream> 
//������������ 4. ���� ������, ���������� ���������, ��������� ���������� (�������) 
void ceasars(char* arr, int n)
{
	int key = 1;
	int code;

	for (int i = 0; i < n; i++)
	{
		code = arr[i] + key;
		if (code > 122)
		{
			code = code - 26;
		}
		arr[i] = char(code);
	}
}

void bubbleSort(int* arr, int n)
{
	int i, j;
	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
}

void shakerSort(int* arr, int n)
{
	for (int i = 0; i < n; i++) {
		int pn = 0;
		int kn = 9;
		for (int j = pn; j < kn; j++) {
			if (arr[pn] > arr[pn + 1]) {
				int temp = arr[pn];
				arr[pn] = arr[pn + 1];
				arr[pn + 1] = temp;
			}
			pn = pn + 1;
			if (arr[kn - 1] > arr[kn]) {
				int temp = arr[kn - 1];
				arr[kn - 1] = arr[kn];
				arr[kn] = temp;
			}
			kn = kn - 1;
		}
	}
}

void outputArray(int* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		std::cout << arr[i] << ' ';
	}
}

void outputCharArray(char* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		std::cout << arr[i];
	}
}

int main()
{
	//������� 1 
	std::cout << "\nFirst\n" << std::endl;
	std::cout << "int pointer size = " << sizeof(int) << std::endl;
	std::cout << "double pointer size = " << sizeof(double) << std::endl;
	std::cout << "char pointer size = " << sizeof(char *) << std::endl;
	std::cout << "long pointer size = " << sizeof(long *) << std::endl;
	std::cout << "bool pointer size = " << sizeof(bool *) << std::endl;

	//������� 2 ���� ������ 
	std::cout << "\nCesar\n" << std::endl;
	const int leng0 = 10;
	char arr[leng0] = { 'h','e','l','l','o','w','o','r','l','d' };
	std::cout << "Original: ";
	outputCharArray(arr, leng0);
	std::cout << std::endl;
	ceasars(arr, leng0);
	std::cout << "Encrypted: ";
	outputCharArray(arr, leng0);
	std::cout << std::endl;

	//������� 3 ���������� ��������� 
	std::cout << "\nBubble sort\n" << std::endl;
	const int leng1 = 10;
	int str[leng1] = { 37,84,62,91,11,65,57,28,19,49 };
	std::cout << "Original: ";
	outputArray(str, leng1);
	std::cout << std::endl;
	bubbleSort(str, leng1);
	std::cout << "Sorted: ";
	outputArray(str, leng1);
	std::cout << std::endl;

	//������� 4 ��������� ���������� 
	std::cout << "\nSheyker sort\n" << std::endl;
	const int length2 = 10;
	int arr2[length2] = { 32,12,43,15,21,9,17,1,19,25 };
	std::cout << "Original: ";
	outputArray(arr2, length2);
	std::cout << std::endl;
	shakerSort(arr2, length2);
	std::cout << "Sorted: ";
	outputArray(arr2, length2);
	std::cout << std::endl;
}
