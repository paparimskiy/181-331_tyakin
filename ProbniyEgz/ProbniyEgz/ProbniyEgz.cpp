#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>


class MassOfSimbols
{
private:
	int n; //������ ������� ��������
	char *mass; //��������� �� ������

public:
	//����������� ��� ����������
	MassOfSimbols()
	{
		n = 0;
		mass = new char[n];
	}
	//����������, � ������� � ������ ������ ���������� ������ �� ���������
	MassOfSimbols(char array[], int length)
	{
		n = length;
		mass = new char[n];
		for (int i = 0; i < n; i++)
		{
			mass[i] = array[i];
		}
	}
	//�����������, ������� �������� ���������� �� ������� ������������ ������
	MassOfSimbols(const MassOfSimbols & m)
	{
		n = m.n;
		mass = new char[n];
		for (int i = 0; i < n; i++)
		{
			mass[i] = m.mass[i];
		}
	}
	//�����������, ���������� ���������� �� std::string;
	MassOfSimbols(std::string s)
	{
		n = s.length(); //���������� ������ ������
		mass = new char[n];//�������� ������ ��� �������
		for (int i = 0; i < n; i++)
		{
			mass[i] = s[i];
		}
	}
	//����������
	~MassOfSimbols()
	{
		if (mass)
		{
			delete[] mass;
		}
	}
	char* getMass()
	{
		return mass;
	}
	int getN()
	{
		return n;
	}
	//������������ ���� ������
	void show()
	{
		std::cout << "Count of simbols = " << n << std::endl;
		for (int i = 0; i < n; i++)
		{
			std::cout << mass[i] << std::endl;
		}
	}
	//���������� ����� ������ � ����
	void clear()
	{
		n = 0;
		delete[] mass;
		mass = new char[n];
	}
	//����� add() ��������� ������� � ����� �������
	void add(std::string s)
	{
		std::string myString(mass, n); //�������� ���������� ������ mass � ������ myString
		delete[] mass;
		n = n + s.length();
		mass = new char[n];
		for (int i = 0; i < n; i++)
		{
			if (i < myString.length())
			{
				mass[i] = myString[i];
			}
			else
			{
				mass[i] = s[i - myString.length()];
			}
		}
	}
	void add(char array[], int length)
	{
		std::string myString(mass, n);
		delete[] mass;
		n = n + length;
		mass = new char[n];
		for (int i = 0; i < n; i++)
		{
			if (i < myString.length())
			{
				mass[i] = myString[i];
			}
			else
			{
				mass[i] = array[i - myString.length()];
			}
		}

	}
	//��������� �� �������� ������� ������ ������ ��������� � ���������� ������������������ ��������
	void insert(std::string s, int pozition)
	{
		std::string myString(mass, n);
		myString.insert(pozition - 1, s);
		delete[] mass;
		n = n + s.length();
		mass = new char[n];
		for (int i = 0; i < n; i++)
		{
			mass[i] = myString[i];
		}
	}
	void insert(char array[], int length, int pozition)
	{
		std::string myString(mass, n);
		std::string s(array, length);
		myString.insert(pozition - 1, s);
		delete[] mass;
		n = n + length;
		mass = new char[n];
		for (int i = 0; i < n; i++)
		{
			mass[i] = myString[i];
		}
	}
	//������� � �������� ������� �������� � ���������� ���������� ��������
	void cut(int pozition, int count)
	{
		std::string myString(mass, n);
		delete[] mass;
		myString.erase(pozition - 1, count);
		n = n - count;
		mass = new char[n];
		for (int i = 0; i < n; i++)
		{
			mass[i] = myString[i];
		}
	}
	//������������ ����� ������������������ �������� (*char) ���� ������ (std::sting) � ������ ������
	void find(std::string s)
	{
		std::string myString(mass, n);
		int p = myString.find(s);
		if (p == -1)
		{
			std::cout << "Not found!" << std::endl;
		}
		else
		{
			std::cout << "This string start with simbol number " << p + 1 << std::endl;
		}
	}
	void find(char array[], int length)
	{
		std::string myString(mass, n);
		std::string s(array, length);
		int p = myString.find(s);
		if (p == -1)
		{
			std::cout << "Not found!" << std::endl;
		}
		else
		{
			std::cout << "This string start with simbol number " << p + 1 << std::endl;
		}
	}
	//��� ������ � ����
	void save(std::string fileName)
	{
		std::string myString(mass, n);
		std::ofstream fout(fileName);
		fout << myString << std::endl;
		fout.close();
	}
	//��� ������ �� �����
	void load(std::string fileName)
	{
		std::ifstream fin(fileName);
		if (!fin)
		{
			std::cout << "File not open!" << std::endl;
		}
		else
		{
			std::string s;
			std::getline(fin, s);
			n = s.length();
			delete[] mass;
			mass = new char[n];
			for (int i = 0; i < n; i++)
			{
				mass[i] = s[i];
			}
		}
	}
};

//���������� ��������� ����
MassOfSimbols operator + (MassOfSimbols &obj1, MassOfSimbols &obj2)
{
	int n = obj1.getN() + obj2.getN();
	char *m = new char[n];
	for (int i = 0; i < n; i++)
	{
		if (i < obj1.getN())
		{
			m[i] = obj1.getMass()[i];
		}
		else
		{
			m[i] = obj2.getMass()[i - obj1.getN()];
		}
	}
	MassOfSimbols obj(m, n);
	return obj;
}

//���������� ��������� <<
std::ostream& operator << (std::ostream &s, MassOfSimbols obj)
{
	std::string str(obj.getMass(), obj.getN());
	s << "Count of simbols = " << obj.getN() << std::endl << "Array (string): " << str << std::endl;
	return s;
}

int main()
{
	//�������� �����������, � ������� � ������ ������ ���������� ������ �� ���������
	char array[] = { 'a', 's', 's', 'd' };
	int count = sizeof(array) / sizeof(char);
	MassOfSimbols myObject1(array, count);
	myObject1.show();

	//�������� ������������ �����������
	MassOfSimbols m(myObject1);
	m.show();

	//�������� ������������, ����������� ���������� �� std::string;
	std::string str = "dkthdnsl222";
	MassOfSimbols myObject2(str);
	myObject2.show();

	//�������� ������ clear()
	myObject1.clear();
	myObject1.show();

	//�������� ������ add(string s), add(char array[], int length)
	MassOfSimbols myObject3("mybook");
	myObject3.add("ku-ku");
	myObject3.show();
	myObject3.add(array, count);
	myObject3.show();

	//�������� ������ insert()
	MassOfSimbols myObject4("my_mother");
	myObject4.insert("dad_and_", 4);
	myObject4.show();
	myObject4.insert(array, count, 2);
	myObject4.show();

	//�������� ������ cut()
	MassOfSimbols myObject5("my_mother_and_dad");
	myObject5.cut(4, 11);
	myObject5.show();

	//�������� �� ������������ �� ������ ������ (� ������ debug)
	MassOfSimbols myObject6 = MassOfSimbols();
	for (int i = 0; i < 10000; i++)
	{
		myObject6.add("ku-ku-ku-ku");
		myObject6.insert("dad_and_", 4);
		myObject6.clear();
	}

	//�������� ���������� ��������� +
	MassOfSimbols myObject7("name");
	MassOfSimbols myObject8("Kate");
	(myObject7 + myObject8).show();

	//�������� ���������� ��������� <<
	std::cout << myObject8;

	//�������� ������ find()
	MassOfSimbols myObject9("123456789");
	myObject9.find("567");
	myObject9.find("ghgj");
	MassOfSimbols myObject10("123456assd789");
	myObject10.find(array, count);

	//�������� ������ save()
	myObject5.save("file1.txt");

	//�������� ������ load()
	myObject10.load("file.txt");
	std::cout << myObject10;

	return 0;
}