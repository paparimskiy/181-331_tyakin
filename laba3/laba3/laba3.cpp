#include "stdafx.h"
#include <iostream>


int main()
{
	std::cout << "Lab_3\n\n";

	int a;
	double dbl;
	int *ptr_int = &a;
	double *ptr_dbl = &dbl;

	std::cout << "1." << std::endl;

	std::cout << "int pointer size = " << sizeof(ptr_int) << std::endl;
	std::cout << "double pointer size = " << sizeof(ptr_dbl) << std::endl;
	std::cout << "char pointer size = " << sizeof(char *) << std::endl;
	std::cout << "long long pointer size = " << sizeof(long long *) << std::endl;
	std::cout << "bool pointer size = " << sizeof(bool *) << std::endl;


	std::cout << "2." << std::endl;

	const char letters[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
		'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

	std::cout << "Start text: ceasar" << std::endl;
	char text[] = "ceasar";

	int r = sizeof(text);

	for (int n = 0; n < r; n++)
	{
		for (int i = 0; i < 26; i++)
		{
			if (letters[i] == text[n])
			{
				if (i >= 23)
				{
					text[n] = letters[i - 23];
					break;
				}

				else
				{
					text[n] = letters[i + 3];
					break;
				}

			}

		}
	}

	std::cout << "Final text: " << text << std::endl;


	std::cout << "3." << std::endl;

	int o;
	int arr1[5] = { 4,2,5,1,3 };
	int w = sizeof(arr1) / sizeof(int);
	
	for (int r = 0; r < w; r++)
	{
		std::cout << arr1[r] << "\t";
	}
	for (int i = 0; i < w; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (arr1[j] > arr1[j + 1])
			{
				o = arr1[j];
				arr1[j] = arr1[j + 1];
				arr1[j + 1] = o;
			}
		}
	}
	std::cout << std::endl;
	for (int r = 0; r < w; r++)
	{
		std::cout << arr1[r] << "\t";
	}
	
	std::cout << std::endl;

	std::cout << "4." << std::endl;

	int arr3[10] = { 8,3,7,5,9,1,4,0,2,6 };
	int e = sizeof(arr3) / sizeof(int);

	for (int r = 0; r < e; r++)
	{
		std::cout << arr3[r]<< "\t";
	}
	std::cout << std::endl;
	for (int i = 0; i < 5; i++) {
		int n = 0;
		int l = 9;
		for (int j = n; j < l; j++)
		{
			if (arr3[n] > arr3[n + 1])
			{
				int t = arr3[n];
				arr3[n] = arr3[n + 1];
				arr3[n + 1] = t;
			}
			n = n + 1;
			if (arr3[l - 1] > arr3[l])
			{
				int t = arr3[l - 1];
				arr3[l - 1] = arr3[l];
				arr3[l] = t;
			}
			l = l - 1;
		}
	}
	
	for (int r = 0; r < e; std::cout << arr3[r] << "\t", r++)
	{
		
	}
	std::cout << std::endl;

	return 0;
}

