#include "stdafx.h"
#include <iostream>

int main()
{
	std::cout << "lab_1" << std::endl;
	int long long a = 310;
	std::cout << "a^4= " << a*a*a*a << std::endl;
	std::cout << "a^5= " << a*a*a*a*a << std::endl;
	int c = 0x5ABC;
	std::cout << "5ABC= " << c << std::endl;
	std::cout << "'b' + 37= " << 'b' + 37 << std::endl;
	std::cout << "53 + 'b'= " << 53 + 'b' << std::endl;
	return 0;
}

