#include "stdafx.h"
#include <iostream>

void swap(int *arr, int i)
{
	int e;
	e = arr[i];
	arr[i] = arr[i - 1];
	arr[i - 1] = e;
}

class sorting
{
public:
	void bubble(int *arr);
	void shaker(int *arr);
	sorting();
	const int m = 10;
private:
	int data[10];
public:
	void setAll(int *arr, int size);
	void printArray();
};

sorting::sorting() 
{
	for (int i = 0; i < 10; i++) 
	{
		data[i] = 0;
	};
}

void sorting::setAll(int *arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		data[i] = arr[i];
	}
}

void sorting::printArray()
{
	for (int i = 0; i < 10; i++)
	{
		std::cout << data[i] << " ";
	}
	std::cout << std::endl;
}

void sorting::bubble(int *arr)
{
	for (int c = 0; c < 10; c++) 
	{
		for (int k = 0; k < 9; k++) 
		{
			if (arr[k] > arr[k + 1]) 
			{
				int j = arr[k];
				arr[k] = arr[k + 1];
				arr[k + 1] = j;
			}
		}
	}
	std::cout << "Bubble-Sorted: ";
	for (int i = 0; i < 10; i++)
	{
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
}

void sorting::shaker(int *arr)
{
	for (int i = 0; i < 5; i++)
	{
		int n = 0;
		int l = 9;
		for (int j = n; j < l; j++)
		{
			if (arr[n] > arr[n + 1])
			{
				int t = arr[n];
				arr[n] = arr[n + 1];
				arr[n + 1] = t;
			}
			n = n + 1;
			if (arr[l - 1] > arr[l])
			{
				int t = arr[l - 1];
				arr[l - 1] = arr[l];
				arr[l] = t;
			}
			l = l - 1;
		}
	}
	std::cout << "Shaker-Sorted: ";
	for (int i = 0; i < 10; i++)
	{
		std::cout << arr[i] << " ";
	}
	std::cout << std::endl;
}

int main()
{
	std::cout << "lab5\n" << std::endl;
	int aray1[10] = { 33, 0, 35, 27, 5, 18, 7, 28, 24, 9 };
	int aray2[10] = { 0, 3, 25, 12, 8, 26, 11, 39, 5, 38 };
	std::cout << "Sort arr1 with bubble-sorted" << std::endl;
	sorting arr1;
	arr1.setAll(aray1, 10);
	arr1.printArray();
	arr1.bubble(aray1);
	std::cout << "Sort arr2 with shaker-sorted" << std::endl;
	sorting arr2;
	arr2.setAll(aray2, 10);
	arr2.printArray();
	arr2.shaker(aray2);
	return 0;
}

