#include <iostream>
#include <openssl/conf.h> 
#include <openssl/conf.h>
#include <openssl/evp.h> 
#include <openssl/err.h> 
#include <openssl/aes.h>
#include <fstream>


#pragma comment (lib, "ws2_32.LIB")
#pragma comment (lib, "gdi32.LIB")
#pragma comment (lib, "advapi32.LIB")
#pragma comment (lib, "crypt32")
#pragma comment (lib, "user32")
#pragma comment (lib, "wldap32")


using namespace std;

int main()
{
	cout << "Crypting..." << endl;
	cout << endl;

	unsigned char *key = (unsigned char *)"0123456789";
	unsigned char *iv = (unsigned char *)"0123456789012345";
	unsigned char plaintext[256];
	unsigned char cryptedtext[256];
	unsigned char decryptedtext[256];


	EVP_CIPHER_CTX *ctx;

	fstream fdef("def.txt", ios::binary | ios::in);
	fstream fenc("enc.txt", ios::binary | ios::out | ios::in | ios::trunc);
	fstream fdec("dec.txt", ios::binary | ios::out | ios::trunc);



	ctx = EVP_CIPHER_CTX_new();
	EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv);
	int len = 0;
	fdef.read((char*)plaintext, 256);
	while (fdef.gcount() > 0)
	{
		EVP_EncryptUpdate(ctx, cryptedtext, &len, plaintext, fdef.gcount());

		fenc.write((char*)cryptedtext, len);

		fdef.read((char*)plaintext, 256);
	}
	EVP_EncryptFinal_ex(ctx, cryptedtext, &len);
	fenc.write((char*)cryptedtext, len);
	fenc.close();
	fdef.close();

	cout << "Crypted!" << endl;
	cout << endl;


	cout << "Decrypting..." << endl;
	cout << endl;
	fenc.open("enc.txt", ios::in | ios::binary);
	if (!(fenc.is_open()))
		return 0;
	ctx = EVP_CIPHER_CTX_new();
	EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv);
	len = 0;
	fenc.read((char*)cryptedtext, 256);
	while (fenc.gcount() > 0)

	{

		EVP_DecryptUpdate(ctx, decryptedtext, &len, cryptedtext, fenc.gcount());


		fdec.write((char*)decryptedtext, len);


		fenc.read((char*)cryptedtext, 256);
	}


	EVP_DecryptFinal_ex(ctx, decryptedtext, &len);
	fdec.write((char*)decryptedtext, len);
	fdec.close();
	fenc.close();

	cout << "Decrypted!" << endl;
	cout << endl;

	system("pause");
	return 0;
}
