//������������ 7. ������������ �������, �������. 
#include "stdafx.h"
#include <iostream> 

class dynamatrix
{
protected:
	int rows;
	int columns;
	double *matr;
public:
	dynamatrix();
	~dynamatrix();
	virtual void create();
	virtual void input();
	void print();
	int getrows();
	int getcolumns();
	bool add(dynamatrix matr2);
	bool multiply(dynamatrix matr2);
	void transpose();
	double getelem(int r, int c)
	{
		return matr[r*columns + c];
	}
	void remove();
};


dynamatrix::dynamatrix()
{
}

dynamatrix::~dynamatrix()
{
}

void dynamatrix::remove()
{
	delete matr;
}

void dynamatrix::create()
{
	int rs;
	int cs;
	std::cout << "Rows = ";
	std::cin >> rs;
	std::cout << "Columns = ";
	std::cin >> cs;
	std::cout << "\n";
	rows = rs;
	columns = cs;
	matr = new double[rows*columns];
}

void dynamatrix::input()
{
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < columns; j++)
		{
			std::cout << "Matr[" << i + 1 << "][" << j + 1 << "] = ";
			std::cin >> matr[i*columns + j];
		}
	std::cout << "\n";
}

void dynamatrix::print()
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			std::cout << matr[i*columns + j] << "\t";
		}
		std::cout << std::endl;
	}
	std::cout << "\n";
}

int dynamatrix::getrows()
{
	return rows;
}

int dynamatrix::getcolumns()
{
	return columns;
}

bool dynamatrix::add(dynamatrix matr2)
{
	if (rows < matr2.getrows())
	{
		std::cout << "Error" << std::endl;
		return false;
	}
	if (columns < matr2.getcolumns())
	{
		std::cout << "Error" << std::endl;
		return false;
	}
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matr[i*columns + j] += matr2.getelem(i, j);
		}

	}
	return true;
}

bool dynamatrix::multiply(dynamatrix matr2)
{
	if (columns != matr2.getrows())
	{
		std::cout << "ERROR \n" << std::endl;
		return false;
	}

	double *matr3 = new double[rows*matr2.getcolumns()];

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matr3[i*columns + j] = matr[i*columns + j];
		}
	}
	int temp = columns;
	columns = matr2.getcolumns();
	for (int j = 0; j < matr2.getcolumns(); j++)
	{
		for (int i = 0; i < rows; i++)
		{
			double x = 0;
			for (int t = 0; t < temp; t++)
			{
				x += matr3[i*temp + t] * matr2.getelem(t, j);
			}
			matr[i*columns + j] = x;
		}
	}
	delete matr3;
	return true;
}

void dynamatrix::transpose()
{
	double * matr3 = new double[rows*columns];

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matr3[i*columns + j] = matr[i*columns + j];
		}
	}

	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matr[j*rows + i] = matr3[i*columns + j];
		}
	}
	int temp = rows;
	rows = columns;
	columns = temp;
	delete matr3;
}


class vectord : public dynamatrix
{
public:

	vectord();
	double ScalarMultiplication(vectord vec2);
	void xmult(double k);
	virtual void create();
	virtual void input();
	double summvectord(vectord vec2);
};

void vectord::create()
{
	int rs = 1;
	int cs;
	std::cout << "Rows = 1" << std::endl;
	std::cout << "Columns = ";
	std::cin >> cs;
	columns = cs;
	rows = rs;
	matr = new double[rows*columns];
}

void vectord::input()
{
	for (int i = 0; i < rows; i++)
		for (int j = 0; j < columns; j++)
		{
			std::cout << "Vec[" << j + 1 << "] = ";
			std::cin >> matr[i*columns + j];
		}
	std::cout << "\n";
}

double vectord::summvectord(vectord vec2)
{
	vectord tmp = *this;
	if (tmp.add(vec2))
	{
		return tmp.getelem(0, 0);
	}
	tmp = *this;
	tmp.transpose();
	if (tmp.add(vec2))
	{
		return tmp.getelem(0, 0);
	}
	return -1;
}

vectord::vectord()
{
}

double vectord::ScalarMultiplication(vectord vec2)
{
	vectord tmp = *this;
	if (tmp.multiply(vec2))
	{
		return tmp.getelem(0, 0);
	}
	tmp = *this;
	tmp.transpose();
	if (tmp.multiply(vec2))
	{
		return tmp.getelem(0, 0);
	}
	return -1;
}

void vectord::xmult(double k)
{
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			matr[i*columns + j] *= k;
		}
	}

}


int main()
{
	dynamatrix A;
	dynamatrix B;
	std::cout << "Input A \n" << std::endl;
	A.create();
	A.input();
	std::cout << "A = \n" << std::endl;
	A.print();
	std::cout << "Input B \n" << std::endl;
	B.create();
	B.input();
	std::cout << "B = \n" << std::endl;
	B.print();
	A.add(B);
	std::cout << "A + B = \n" << std::endl;
	A.print();
	A.transpose();
	std::cout << "A + B transposed = \n" << std::endl;
	A.print();
	A.multiply(B);
	std::cout << "A + B transposed * B = \n" << std::endl;
	A.print();

	vectord C;
	vectord D;
	std::cout << "Input vector C \n" << std::endl;
	C.create();
	C.input();
	std::cout << "vector C: \n" << std::endl;
	C.print();
	std::cout << "Input vector D \n" << std::endl;
	D.create();
	D.input();
	std::cout << "vector D = \n" << std::endl;
	D.print();
	C.add(D);
	std::cout << "vector C + vector D = \n" << std::endl;
	C.print();
	C.transpose();
	std::cout << "vector C + vector D transposed: \n" << std::endl;
	C.print();

	A.remove();
	B.remove();
	C.remove();
	D.remove();
	return 0;
}
