#include "stdafx.h"
#include <iostream>


int main()
{
	std::cout << "lab2\n" << std::endl;
	/*
	\b delete previous symbol
	\n get to the new string
	\t use tabullation
	\\ print: \
	\" print: "
	\' print: '
	*/
	std::cout << "Using \\b: \t" << "\"Hello, World!\b\"" << std::endl;
	std::cout << "Using \\t: \t" << "\"Hello, World!\t\"" << std::endl;
	std::cout << "Using \\n: \t" << "\"Hello,\n World!\"" << std::endl;
	std::cout << "Using \\: \t" << "\"Hello, World!\\\"" << std::endl;
	std::cout << "Using \": \t" << "\"Hello, World! \" \" " << std::endl;
	std::cout << "Using \': \t" << "\"Hello, World!\' \"" << std::endl;

	return 0;
}